import time


accepted = ['1', '2', '3', 'end']


def graph(tab):
    print('-------------')
    print('|', tab[0][0], '|', tab[0][1], '|', tab[0][2], '|')
    print('|-----------|')
    print('|', tab[1][0], '|', tab[1][1], '|', tab[1][2], '|')
    print('|-----------|')
    print('|', tab[2][0], '|', tab[2][1], '|', tab[2][2], '|')
    print('-------------')


def player1_move(tab):
    x = input('Row: ')
    while x not in accepted:
        print("Enter the correct number!")
        x = input('Row: ')
    if x == 'end':
        print('')
        return menu()
    y = input('Column: ')
    while y not in accepted:
        print("Enter the correct number!")
        y = input('Column: ')
    if y == 'end':
        print('')
        return menu()
    x = int(x)
    y = int(y)
    if tab[x-1][y-1] == ' ':
        tab[x-1][y-1] = 'X'
    else:
        print("You can't make your move here.")
        return player1_move(tab)


def player2_move(tab):
    x = input('Row: ')
    while x not in accepted:
        print("Enter the correct number!")
        x = input('Row: ')
    if x == 'end':
        print('')
        return menu()
    y = input('Column: ')
    while y not in accepted:
        print("Enter the correct number!")
        y = input('Column: ')
    if y == 'end':
        print('')
        return menu()
    x = int(x)
    y = int(y)
    if tab[x-1][y-1] == ' ':
        tab[x-1][y-1] = 'O'
    else:
        print("You can't make your move here.")
        return player2_move(tab)


def win(tab):
    is_win = False
    for i in range(3):
        if tab[i][0] == 'X' and tab[i][1] == 'X' and tab[i][2] == 'X':
            is_win = True
            break
        if tab[0][i] == 'X' and tab[1][i] == 'X' and tab[2][i] == 'X':
            is_win = True
            break
    if tab[0][0] == 'X' and tab[1][1] == 'X' and tab[2][2] == 'X':
        is_win = True
    if tab[0][2] == 'X' and tab[1][1] == 'X' and tab[2][0] == 'X':
        is_win = True
    for i in range(3):
        if tab[i][0] == 'O' and tab[i][1] == 'O' and tab[i][2] == 'O':
            is_win = True
            break
        if tab[0][i] == 'O' and tab[1][i] == 'O' and tab[2][i] == 'O':
            is_win = True
            break
    if tab[0][0] == 'O' and tab[1][1] == 'O' and tab[2][2] == 'O':
        is_win = True
    if tab[0][2] == 'O' and tab[1][1] == 'O' and tab[2][0] == 'O':
        is_win = True
    return is_win


def full(tab):
    is_full = False
    if ' ' not in tab[0] and ' ' not in tab[1] and ' ' not in tab[2]:
        is_full = True
    return is_full


def menu():
    print("TIC TAC TOE")
    print("Play (1)     Exit (0)")
    odp = input()
    while odp not in ["0", "1"]:               # error-handling jeśli gracz wciśnie niepożądany klawisz
        print("\nPress the correct key.")
        odp = input()
    if odp == "1":
        game()
    elif odp == "0":
        print("\nGoodbye...")
        time.sleep(1)     # tylko dla kosmetyki, żeby program nie zamykał się od razu
        quit()


def game():
    tab = [[' ', ' ', ' '],
           [' ', ' ', ' '],
           [' ', ' ', ' ']]
    while True:
        print('\nPLAYER 1 MOVE')
        graph(tab)
        player1_move(tab)
        is_win = win(tab)
        if is_win:
            print('')
            graph(tab)
            print("CONGRATULATIONS! Player 1 won.")
            input('Press any key to start a new game.')
            print('')
            return menu()
        is_full = full(tab)
        if is_full:
            print('')
            graph(tab)
            print("DRAW. No more moves avaible.")
            input('Press any key to start a new game.')
            print('')
            return menu()
        print('\nPLAYER 2 MOVE')
        graph(tab)
        player2_move(tab)
        is_win = win(tab)
        if is_win:
            print('')
            graph(tab)
            print("CONGRATULATIONS! Player 2 won.")
            input('Press any key to start a new game.')
            print('')
            return menu()


menu()
