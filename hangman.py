import random
import time
letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
           'w', 'x', 'y', 'z']
words1 = ['happy', 'blue', 'dog', 'breakdance', 'western', 'minecraft', 'concatenate', 'foxtrot',
          'uniform', 'charlie', 'kilogram']
words = ['kitten', 'elephant', 'salmon', 'jaguar', 'giraffe', 'dolphin', 'alligator', 'crocodile', 'cheetah', 'ostrich',
         'kangaroo', 'python', 'panther']
graphic = [['', '', '', '', '_|_'],
           [' |', ' |', ' |', ' |', '_|_'],
           [' ______', ' |', ' |', ' |', ' |', '_|_'],
           [' ______', ' |    |', ' |', ' |', ' |', '_|_'],
           [' ______', ' |    |', ' |    O', ' |', ' |', '_|_'],
           [' ______', ' |    |', ' |    O', ' |    |', ' |', '_|_'],
           [' ______', ' |    |', ' |    O', ' |   /|', ' |', '_|_'],
           [' ______', ' |    |', ' |    O', ' |   /|\\', ' |', '_|_'],
           [' ______', ' |    |', ' |    O', ' |   /|\\', ' |   /', '_|_'],
           [' ______', ' |    |', ' |    O', ' |   /|\\', ' |   / \\', '_|_']]


def random_word():
    number = random.randint(0, 11)
    word = words[number]
    return word


def hidden(word, correct_letters):
    for i in range(len(word)):
        if i < (len(word)-1):
            if word[i] in correct_letters:
                print(word[i], end=' ')
            else:
                print('_', end=' ')
        else:
            if word[i] in correct_letters:
                print(word[i])
            else:
                print('_')


def game(word):
    counter = -1
    correct_letters = []
    wrong_letters = []
    word_letters = []
    for i in word:
        if i not in word_letters:
            word_letters.append(i)
    while True:
        hidden(word, correct_letters)
        letter = input('Try a letter: ')
        if letter in correct_letters or letter in wrong_letters:
            print("You have already tried this letter!")
        elif len(letter) == 1 and letter in word:
            correct_letters.append(letter)
        else:
            if letter in letters:
                print("Wrong!")
                counter += 1
                picture(counter)
                wrong_letters.append(letter)
            elif letter != word:
                print("This is not a letter!")
            if counter == 9:
                picture(counter)
                print("\nGAME OVER")
                print("The word was:", word)
                input('Press any key to return to the menu.\n')
                return menu()
        if letter == word or sorted(word_letters) == sorted(correct_letters):
            print("\nCONGRATULATIONS! You got the word", '\''+word+'\'', "right!")
            input('Press any key to return to the menu.\n')
            return menu()


def picture(counter):
    print('')
    for i in graphic[counter]:
        print(i)


def rules():
    print('\nHANGMAN\nThe game gives you a random hidden word (displayed as "_ _ _ _").')
    print('Your objective is to guess the word before the stick-man is hung.')
    print('If your letter appears in the word, it will be revealed where it\'s placed.')
    print('If your guess is wrong, a new piece of the stick-man will be drawn.')
    print('In each round you can either input a letter or, if you feel confident, a whole word.')
    print('Good luck!')
    input('(Press any key to return to the menu.)\n')
    return menu()


def menu():
    print("HANGMAN")
    print("Play (1)     Rules (2)     Exit (0)")
    odp = input()
    while odp not in ["0", "1", "2"]:
        print("\nPress the correct key.")
        odp = input()
    if odp == "1":
        word = random_word()
        game(word)
    elif odp == "2":
        rules()
    elif odp == "0":
        print("\nGoodbye...")
        time.sleep(1)
        quit()


menu()
